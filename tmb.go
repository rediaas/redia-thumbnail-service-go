package thumbnails

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"bitbucket.org/rediaas/redia-thumbnail-service-go/gcp"

	"gocloud.dev/blob"
)

var (
	// ErrNoSourceBucketDefined is returned during initialization if missing
	ErrNoSourceBucketDefined = errors.New("Source URL (TMB_SOURCE_URL) is not set")
	// ErrNoDestinationBucketDefined is returned during initialization if missing
	ErrNoDestinationBucketDefined = errors.New("Destination URL (TMB_DEST_URL) is not set")
)

// Service represents a thumbnail-service instance
type Service struct {
	sources []url.URL
	destURL *url.URL
	// useSignedURL tells the service if we want to do our own IO, or redirect to blob storage
	useRedirect           bool
	sourceBucketPath      string
	destinationBucketPath string
}

// ServiceOpts configures the service instance
type ServiceOpts struct {
	Sources string
	DstURL  string
}

// OpenBucket is a wrapper for gocloud.dev/blob OpenBucket
func (s *Service) OpenBucket(ctx context.Context, urlstr string) (*blob.Bucket, error) {
	// when we need other providers, this will be smarter
	return gcp.OpenBucket(ctx, urlstr)
}

// New parses env vars and does sanity checks
func New(opt ServiceOpts) (*Service, error) {
	s := Service{
		useRedirect: gcp.UseRedirect(),
	}

	so := strings.Split(opt.Sources, ",")
	sources := make([]url.URL, 1)
	for _, v := range so {
		if v == "" {
			fmt.Println("skipping empty source")
			continue
		}
		u, err := url.Parse(v)
		if err != nil {
			fmt.Printf("Error parsing: '%s'. Skipping source.\n", err.Error())
			continue
		}
		fmt.Println("Source whitelisted:", u.String())
		sources = append(sources, *u)
	}
	s.sources = sources

	dest := opt.DstURL

	if dest == "" {
		return nil, ErrNoDestinationBucketDefined
	}

	destURL, err := url.Parse(dest)
	if err != nil {
		return nil, err
	}

	s.destURL = destURL

	return &s, nil
}

// PreProcessImage processes one asset, with multiple processing-options
func (s *Service) PreProcessImage(a *Asset, opts []ImageOpts) error {
	srcfname, err := a.getSourceImage()
	if err != nil {
		return err
	}
	defer os.Remove(srcfname)

	for _, opt := range opts {
		stime := time.Now()
		fmt.Printf("PUB/SUB: %s %v %s [processed]\n", a.URL.String(), opt, time.Since(stime))
		if err := a.ProcessImage(srcfname, opt, nil); err != nil {
			// for now pre-processing is a feature that can fail, so lets just abort on err
			return err
		}
	}
	return nil
}
