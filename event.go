package thumbnails

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/url"
	"os"
	"os/exec"
	"time"
)

// Event is used to unmarshal GCP pub/sub messages
type Event struct {
	Name   string `json:"name"`
	CRC    string `json:"crc32c"`
	MD5    []byte `json:"md5hash"`
	Bucket string `json:"bucket"`
}

// HandleEvent parses and handles pub/sub events from buckets
func (s *Service) HandleEvent(ctx context.Context, e Event) error {
	// TODO: Make this more generic so we can work with AWS as well
	u, err := url.Parse(fmt.Sprintf("gs://%s/%s", e.Bucket, e.Name))
	if err != nil {
		return err
	}

	a, err := s.NewAsset(ctx, u)
	if err != nil {
		return err
	}

	// Image processing is done for both images and videos
	opts := []ImageOpts{
		ImageOpts{
			Width: 600,
		},
		ImageOpts{
			Width: 960,
		},
		ImageOpts{
			Width: 1280,
		},
		ImageOpts{
			Width: 1920,
		},
	}

	err = s.PreProcessImage(a, opts)
	if err != nil {
		return err
	}

	// If video, we transcode that.
	if a.IsVideo() {
		vopts := []VideoPreset{
			Video720P,
		}
		err := s.PreProcessVideo(a, vopts)
		if err != nil {
			return err
		}
		// Transcode stuff
	}

	// connect to both buckets (and close when done)
	// check if we already have a thumbnail

	return nil
}

// PreProcessVideo processes video assets in the requested presets
func (s *Service) PreProcessVideo(a *Asset, opts []VideoPreset) error {
	srcb, err := a.s.OpenBucket(a.ctx, a.URL.String())
	if err != nil {
		return err
	}
	defer srcb.Close()

	sr, err := srcb.NewReader(a.ctx, a.Name, nil)
	if err != nil {
		return err
	}
	defer sr.Close()

	sf, err := ioutil.TempFile("", "")
	if err != nil {
		return err
	}
	defer sf.Close()
	defer os.Remove(sf.Name())

	_, err = io.Copy(sf, sr)
	if err != nil {
		return err
	}

	// Open destination bucket
	db, err := a.s.OpenBucket(a.ctx, s.destURL.String())
	if err != nil {
		return err
	}
	defer db.Close()

	// Consider putting this into a go-routine, measure
	for _, opt := range opts {
		stime := time.Now()
		fmt.Println()
		df, err := ioutil.TempFile("", "")
		if err != nil {
			return err
		}
		defer df.Close()
		defer os.Remove(df.Name())

		args := opt.ExecArgs(sf.Name(), df.Name())
		prefix := opt.String()
		dname := fmt.Sprintf("%s/%s", prefix, a.Name)

		ex, err := db.Exists(a.ctx, dname)
		if err != nil {
			fmt.Printf("%s already exists, skipping\n", dname)
			return err
		}
		if ex {
			// Skip if we already have a downscaled version
			continue
		}

		w, err := db.NewWriter(a.ctx, dname, nil)
		if err != nil {
			return err
		}
		defer w.Close()

		fmt.Println("debug", args)
		cmd := exec.Command(args[0], args[1:]...)
		//cmd.Stdout = os.Stdout
		var out bytes.Buffer
		cmd.Stderr = &out // todo: store in a buffer and show with error
		err = cmd.Run()

		if err != nil {
			fmt.Println(out.String())
			return err
		}

		_, err = io.Copy(w, df)
		if err != nil {
			return err
		}
		fmt.Printf("PUB/SUB: %s %v %s [transcoded]\n", a.URL.String(), opt, time.Since(stime))
	}

	// Fetch file from bucket
	// Create dest temp file
	// Run ffmpeg
	// Store in cache bucket

	return nil
}
