package thumbnails

// VideoPreset is used by pre-processing to configure ffmpeg
type VideoPreset int

const (
	// Video720P is a preset for 1280x720
	Video720P VideoPreset = iota
)

// ExecArgs tells ffmpeg what to do with presets
func (v *VideoPreset) ExecArgs(sf, df string) []string {
	switch *v {
	case Video720P:
		return []string{
			"ffmpeg", "-y", "-i", sf, "-vf", "scale=1280:720", "-f", "mp4", df,
		}

	default:
		return []string{
			"echo", "unsupported",
		}
	}
}
func (v *VideoPreset) String() string {
	// TODO: Implement other prefixes
	return "720p"
}
