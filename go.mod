module bitbucket.org/rediaas/redia-thumbnail-service-go

go 1.12

require (
	cloud.google.com/go v0.44.1
	contrib.go.opencensus.io/exporter/aws v0.0.0-20190807220307-c50fb1bd7f21 // indirect
	contrib.go.opencensus.io/exporter/ocagent v0.6.0 // indirect
	contrib.go.opencensus.io/exporter/stackdriver v0.12.5 // indirect
	github.com/Azure/azure-amqp-common-go v1.1.4 // indirect
	github.com/Azure/azure-pipeline-go v0.2.2 // indirect
	github.com/Azure/azure-sdk-for-go v32.4.0+incompatible // indirect
	github.com/Azure/azure-storage-blob-go v0.7.0 // indirect
	github.com/Azure/go-autorest v12.4.3+incompatible // indirect
	github.com/Azure/go-autorest/tracing v0.1.0 // indirect
	github.com/GoogleCloudPlatform/cloudsql-proxy v0.0.0-20190725230627-253d1edd4416 // indirect
	github.com/aws/aws-sdk-go v1.22.4 // indirect
	github.com/codahale/hdrhistogram v0.0.0-20161010025455-3a0bb77429bd // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/pprof v0.0.0-20190723021845-34ac40c74b70 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.9.5 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/mattn/go-ieproxy v0.0.0-20190805055040-f9202b1cfdeb // indirect
	github.com/opentracing/opentracing-go v1.0.2 // indirect
	github.com/rogpeppe/fastuuid v1.2.0 // indirect
	github.com/tidwall/pretty v0.0.0-20190325153808-1166b9ac2b65 // indirect
	github.com/uber-go/atomic v1.3.2 // indirect
	github.com/uber/jaeger-client-go v2.15.0+incompatible // indirect
	github.com/uber/jaeger-lib v1.5.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.1 // indirect
	go.opencensus.io v0.22.0
	go.uber.org/atomic v1.3.2 // indirect
	gocloud.dev v0.16.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/mobile v0.0.0-20190806162312-597adff16ade // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/tools v0.0.0-20190813034749-528a2984e271 // indirect
	google.golang.org/genproto v0.0.0-20190801165951-fa694d86fc64
	gopkg.in/yaml.v2 v2.2.2 // indirect
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
	pack.ag/amqp v0.12.0 // indirect
)
