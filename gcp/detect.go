package gcp

import "cloud.google.com/go/compute/metadata"

// UseRedirect uses redirects, if we're on GCE
func UseRedirect() bool {
	return metadata.OnGCE()
}
