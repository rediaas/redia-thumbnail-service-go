// Package gcp does Google specific workarounds
package gcp

import (
	"context"
	"log"
	"net/url"

	credentials "cloud.google.com/go/iam/credentials/apiv1"
	"gocloud.dev/blob"
	"gocloud.dev/blob/gcsblob"
	"gocloud.dev/gcp"
	"golang.org/x/oauth2/google"
	credentialspb "google.golang.org/genproto/googleapis/iam/credentials/v1"
)

const (
	// todo: try to detect this somewhere
	serviceAccount = "tmbserv-gyldendal@redia-internal.iam.gserviceaccount.com"
)

var (
	creds *google.Credentials
)

// OpenBucket does some Google Specific stuff, we need to refactor if we move to another cloud provider
func OpenBucket(ctx context.Context, urlstr string) (*blob.Bucket, error) {
	u, err := url.Parse(urlstr)
	if err != nil {
		return nil, err
	}

	bucketname := u.Hostname()
	signer, err := newBucketSigner(context.Background())
	if err != nil {
		return nil, err
	}
	opt := &gcsblob.Options{
		GoogleAccessID: serviceAccount,
		SignBytes:      signer.sign,
	}

	// Your GCP credentials.
	// See https://cloud.google.com/docs/authentication/production
	// for more info on alternatives.
	if creds == nil {
		creds, err = gcp.DefaultCredentials(ctx)
		if err != nil {
			return nil, err
		}
	}

	// Create an HTTP client.
	// This example uses the default HTTP transport and the credentials
	// created above.
	client, err := gcp.NewHTTPClient(
		gcp.DefaultTransport(),
		gcp.CredentialsTokenSource(creds))
	if err != nil {
		log.Fatal(err)
	}
	return gcsblob.OpenBucket(ctx, client, bucketname, opt)
}

type bucketSigner struct {
	ctx context.Context
	c   credentials.IamCredentialsClient
}

func newBucketSigner(ctx context.Context) (*bucketSigner, error) {
	c, err := credentials.NewIamCredentialsClient(ctx)
	if err != nil {
		return nil, err
	}
	return &bucketSigner{
		ctx: ctx,
		c:   *c,
	}, nil
}

func (s *bucketSigner) sign(b []byte) ([]byte, error) {

	req := &credentialspb.SignBlobRequest{
		Payload: b,
		Name:    serviceAccount,
	}
	resp, err := s.c.SignBlob(s.ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.SignedBlob, err
}
