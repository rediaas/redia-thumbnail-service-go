package thumbnails

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
)

// ScaleByWidth takes a reader, writeer.
// Then creates a temp file, runs vips, then writes the thumbnail to the writer
func ScaleByWidth(w io.Writer, r io.Reader, width int, ext string) error {
	srcf, err := ioutil.TempFile("", "tmb_s_")
	if err != nil {
		return err
	}

	// Temporary source file
	_, err = io.Copy(srcf, r)
	if err != nil {
		return err
	}
	srcf.Sync()

	// Temporary dest file
	// File-type is decided here. The PNG files were too huge to use.
	dstf, err := ioutil.TempFile("", fmt.Sprintf("tmb_d_*.%s", ext))
	if err != nil {
		return err
	}

	cmd := exec.Command("vipsthumbnail", "--size", strconv.Itoa(width), srcf.Name(), "-o", dstf.Name()+"[optimize_coding,strip,background=255]", "--rotate")
	if ext == "gif" || ext == "webp" {
		cmd = exec.Command("vipsthumbnail", "--size", strconv.Itoa(width), srcf.Name()+"[n=-1]", "-o", dstf.Name())
	}
	var out bytes.Buffer
	cmd.Stderr = &out
	cmd.Stdout = &out

	err = cmd.Run()
	if err != nil {
		fmt.Println(out.String())
		return err
	}
	dstf.Sync()

	_, err = io.Copy(w, dstf)
	if err != nil {
		return err
	}

	// Clean up
	defer dstf.Close()
	defer os.Remove(dstf.Name())
	defer os.Remove(srcf.Name())

	return nil
}

// ImageOpts contain options for the processing
type ImageOpts struct {
	Width int
}
