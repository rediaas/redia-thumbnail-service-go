package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	thumbnails "bitbucket.org/rediaas/redia-thumbnail-service-go"

	"gocloud.dev/gcp"
	"gocloud.dev/pubsub"
	"gocloud.dev/server"
	"gocloud.dev/server/sdserver"

	// todo: move anonymous imports into plugins.go
	"go.opencensus.io/trace"
	_ "gocloud.dev/blob/gcsblob"
	_ "gocloud.dev/pubsub/gcppubsub"
)

// GlobalMonitoredResource implements monitoredresource.Interface to provide a
// basic global resource based on the project ID. If you're running this sample
// on GCE or EC2, you may prefer to use monitoredresource.Autodetect() instead.
type GlobalMonitoredResource struct {
	projectID string
}

func (g GlobalMonitoredResource) MonitoredResource() (string, map[string]string) {
	return "global", map[string]string{"project_id": g.projectID}
}

// Parse this at start and return appArgs
type appArgs struct {
	port      int
	pubsubURL string
	srcURL    string
	dstURL    string
	doTrace   bool
}

func main() {
	bgCTX := context.Background()
	args, err := parseArgs()
	if err != nil {
		log.Fatal(err)
	}

	// Initialize thumbnail service
	ts, err := thumbnails.New(thumbnails.ServiceOpts{
		Sources: args.srcURL,
		DstURL:  args.dstURL,
	})
	if err != nil {
		log.Fatal(err)
	}

	// Connect and subscribe to message-queue
	if args.pubsubURL != "" {
		subscription, err := pubsub.OpenSubscription(bgCTX, args.pubsubURL)
		if err != nil {
			log.Fatal(err)
		}
		defer subscription.Shutdown(bgCTX)
		go func() {
			for {
				fmt.Println("Polling for pub/sub messages in the background...")
				msg, err := subscription.Receive(bgCTX)
				if err != nil {
					fmt.Println("pub/sub aborting", err.Error())
					log.Fatal(err)
				}
				var m thumbnails.Event
				err = json.Unmarshal(msg.Body, &m)
				if err != nil {
					fmt.Println(err)
					break
				}

				//fmt.Printf("%s", msg.Body)

				err = ts.HandleEvent(bgCTX, m)

				if err != nil {
					fmt.Println("Error from pubsub", err) // handle this
					fmt.Printf("%s", msg.Body)
					msg.Nack()
					continue // skip the message if it fails
				}
				msg.Ack()
			}
		}()
	}

	// HTTP

	if args.port != 0 {
		var exporter trace.Exporter

		credentials, err := gcp.DefaultCredentials(bgCTX)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("debugging creds: %s %s\n", os.Getenv("GCP_SERVICE_ACCOUNT"))

		projectID, err := gcp.DefaultProjectID(credentials)
		if err != nil {
			log.Fatal(err)
		}

		// debugging starts

		tokenSource := gcp.CredentialsTokenSource(credentials)

		if projectID != "" {
			fmt.Println("Exporting traces to Stackdriver")
			mr := GlobalMonitoredResource{projectID: string(projectID)}
			exporter, _, err = sdserver.NewExporter(projectID, tokenSource, mr)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			fmt.Println("Skipping tracing because ProjectID could not be detected. Running locally?")
		}

		// Enable tracing for a small percentage of our requests by default
		var samplingPolicy trace.Sampler
		samplingPolicy = trace.ProbabilitySampler(0.05)
		if args.doTrace {
			samplingPolicy = trace.AlwaysSample()
		}

		options := &server.Options{
			RequestLogger:         sdserver.NewRequestLogger(),
			TraceExporter:         exporter,
			DefaultSamplingPolicy: samplingPolicy,
			Driver:                &server.DefaultDriver{},
		}
		srv := server.New(http.DefaultServeMux, options)

		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, "tmbserv says hello")
		})

		http.HandleFunc("/img/", ts.ImageHandler)
		http.HandleFunc("/video/", ts.VideoHandler)
		fmt.Printf("Listening on port %d...\n", args.port)
		log.Fatal(srv.ListenAndServe(fmt.Sprintf(":%d", args.port)))
	}
}

func parseArgs() (*appArgs, error) {
	var port = flag.Int("port", 8080, "Runs a HTTP server on this port. Required for web-server.")
	var pubsubURL = flag.String("pubsub", "", "Connection string for pub/sub. Required for on-upload processing.")
	var srcURL = flag.String("src", "", "Storage buckets for source-assets. E.g. gs://mybucket/mydir. Multiple buckets can be seperated by comma. Required.")
	var dstURL = flag.String("dst", "", "Storage bucket for cache assets. E.g. gs://mycachebucket/mydir. Required.")
	var doTrace = flag.Bool("trace", false, "Enable tracing on all requests on Stackdriver")

	flag.Parse()

	if *srcURL == "" || *dstURL == "" || *port == 0 {
		// Lazy. Either use args or env. Not both
		if os.Getenv("PORT") == "" {
			p, err := strconv.Atoi(os.Getenv("PORT"))
			if err == nil {
				*port = p
			}
		}

		*srcURL = os.Getenv("TMB_SRC")
		*dstURL = os.Getenv("TMB_DST")
	}

	// TODO: Maybe grab a config package to deal with flags/env vars
	if *pubsubURL == "" {
		*pubsubURL = os.Getenv("TMB_PUBSUB")
	}

	if *srcURL == "" || *dstURL == "" {
		fmt.Fprintf(os.Stderr, "No src or dst defined\nReceived\n%s\n%s\n", *srcURL, *dstURL)
		flag.Usage()
		return nil, errors.New("Invalid args")
	}

	if *port == 0 && *pubsubURL == "" {
		fmt.Fprintf(os.Stderr, "You need to define -port for web-server, and/or -pubsub for pub/sub support. None were defined.\n")
		flag.Usage()
		return nil, errors.New("Invalid args")
	}

	return &appArgs{
		port:      *port,
		pubsubURL: *pubsubURL,
		srcURL:    *srcURL,
		dstURL:    *dstURL,
		doTrace:   *doTrace,
	}, nil

}
