## Roadmap

### Phase 1

Proof of concept. Recovering from this phase will require som code-cleanup, testing and refactoring.

1. [x] Service can run via Cloud Run
1. [x] Service has access to image-processing binaries
1. [x] Service can connect to Google services through service-account
1. [x] Service can read files from source-bucket
1. [x] User can get a thumbnail by width by referencing an image from a bucket
1. [x] Service can parse options in image-url
1. [x] Service can convert a filename + options to a filename that we can use to store thumbnails with
1. [x] Service can write files from destination-bucket
1. [x] User can get a pre-processed thumbnail from service
1. [x] Service can upload to Google Cloud Storage bucket
1. [x] Bucket can be set up to send storage-updates to pub/sub
1. [x] Service can listen for storage-updates
1. [x] Service can initiate processing on-upload event
1. [x] Service creates `600px 960px 1280px 1920px` thumbnails on-upload to match Material-UI breakpoints
1. [x] Organize the code a bit. Go mod is forcing me to use one file for the entire library.
1. [x] User can get a thumbnail by width by referencing a video from a bucket
1. [ ] Can be run as either http server, pub-sub listener or both
1. [ ] HEAD call for the frontend to check if the resource exists

### Phase 2: Videoq

1. [x] Service can return pre-processed 720p versions of videos
1. [x] Gif support. Ønske fra Kasper
1. [x] Service will return full quality versions if no pre-processed version exist
1. [ ] Bypass the 32M response limit on cloud-run by redirecting to a signed URL (https://godoc.org/gocloud.dev/blob#Bucket.SignedURL)
1. [ ] Service will schedule pre-processing, if it gets a miss on a video version
1. [ ] Set timeout on cloud-run so that we can finish ffmpeg processing
1. [-] Handle graceful exit, if someone tries to shut us down. Update: cloud-run doesn't do graceful exit, so useless to try
1. [ ] Maybe handle subscriptions by push
1. [ ] Roll gif-support back and simply pipe the gifs instead of resizing them
1. [ ] Listen for DELETE events and delete cache related to it

```
gcloud beta pubsub subscriptions create tmb-gyldendal-invoked --topic projects/redia-internal/topics/tmb-storage-events --push-endpoint=https://tmbserv-redia-acyeuzbsfq-ew.a.run.app/ --push-auth-service-account=cloud-run-pubsub-invoker@redia-internal.iam.gserviceaccount.com
```

### Maybe-later

1. [ ] Use e-tag to hash thumbnails with so updated files also get thumbnails
1. [ ] Use Redis for caching instead of buckets
1. [ ] Bloomfilter can tell us if we have a file before we ask the bucket
1. [ ] Use redirect instead of piping data to and from buckets, maybe. See: https://stackoverflow.com/questions/22759710/setting-up-ssl-for-google-cloud-storage-static-website
1. [ ] More processing options (like crop)
