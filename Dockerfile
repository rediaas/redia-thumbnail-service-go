# libvips
FROM alpine:latest as vipsbuild
ENV VIPS_VERSION = 8.8.1
RUN apk --no-cache add build-base pkgconf glib-dev \
    giflib-dev libjpeg-turbo-dev libpng-dev libexif-dev \
    expat-dev libwebp-dev imagemagick6-dev \
    librsvg-dev
RUN wget https://github.com/libvips/libvips/releases/download/v8.8.1/vips-8.8.1.tar.gz -O /tmp/vips.tgz && cd /tmp && tar xvfz vips.tgz
RUN cd /tmp/vips-8.8.1 && \
    CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0" ./configure --disable-debug --disable-docs --disable-static --disable-introspection --disable-dependency-tracking --enable-cxx=yes --without-python --without-orc --without-fftw --prefix=/opt/vips && \
    make && make install

FROM golang:1.12 as builder

WORKDIR /app

# Copy the app and build
COPY . /app
RUN CGO_ENABLED=0 GOOS=linux go build -mod=vendor cmd/tmbserv/tmbserv.go

# CXXFLAGS="-D_GLIBCXX_USE_CXX11_ABI=0" ./configure --disable-debug --disable-docs --disable-static --disable-introspection --disable-dependency-tracking --enable-cxx=yes --without-python --without-orc --without-fftw $1
# make && make install


# Multi-stage binary container from alpine
FROM alpine:latest
RUN apk --no-cache add ca-certificates ffmpeg mailcap \
    giflib libjpeg glib giflib imagemagick6-libs libjpeg-turbo expat libexif libgsf librsvg libwebp
WORKDIR /app
COPY --from=builder /app/tmbserv .
COPY --from=vipsbuild /opt/vips /opt/vips
RUN ln -s /opt/vips/bin/vipsthumbnail /usr/bin/vipsthumbnail
ENTRYPOINT [ "./tmbserv" ]
