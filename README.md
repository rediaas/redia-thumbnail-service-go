# Tiny Media Box

Or Readias media-service

## Elevator pitch

Dynamically make all images and videos available as responsive-images. Upload your files to a Cloud Storage bucket like usual and reap the benefits by changing one href tag on your img elements.

## Features

### Thumbnails images and videos

### Downscaling of videos

### On-demand processing

### On-upload processing

## Non-Goals

- Generic image processing
- Non-bucket assets

- Website thumbnails (may change later)

## Roadmap

[See ROADMAP here](Roadmap.md)

## Toolbox

The service requires ffmpeg and vips to be installed

### libvips

Fastest image processing library out there.

- https://jcupitt.github.io/libvips/
- https://github.com/h2non/bimg/blob/master/preinstall.sh

### ffmpeg

Used to extract screenshots from video files

### puppeteer

Headless Chrome to do screenshots of websites

## Process Flow

1. Thumbnail requested (either by upload or on-demand)
1. Get or create source image
   - images are their own source-image
   - videos and websites need a middle-step here
1. Translate requested filename to internal-asset-name
1. Check if already exists
1. Serve image

## DevOps

Install Google's cloud SDK on your machine

### Configure

Set project-id

```sh
# Staging & Testing
gcloud config set project gyldendal-flexafvikler
# Production
gcloud config set project gyldendal-teknologi-prod
```

Set default region for cloud-run

```sh
gcloud config set run/region europe-west1
```

### Build

#### Cloud build

```sh
gcloud config set project redia-internal
gcloud builds submit --tag eu.gcr.io/redia-internal/tmbserver
```

#### Local build

```sh
docker build -t eu.gcr.io/redia-internal/tmbserver
```

Then [push to Google's container registry](https://cloud.google.com/container-registry/docs/pushing-and-pulling)

### Deploy

_Remember_ to set ProjectID correctly when you deploy so that you don't accidentally deploy to production.

```sh
gcloud beta run deploy --platform managed --image eu.gcr.io/redia-internal/tmbserver \
--set-env-vars ^--^TMB_DST=gs://tmbdata.gyldendal.redia.dk--TMB_SRC=gs://gyldendal-flexafvikler.appspot.com/upload,gs://gyldendal-teknologi-prod.appspot.com,gs://gyldendal-teknologi-testing.appspot.com/--TMB_PUBSUB=gcppubsub://redia-internal/tmb-gyldendal \
--memory 1024Mi --region europe-west1 tmbserv-redia \
--service-account tmbserv-gyldendal@redia-internal.iam.gserviceaccount.com
```

### Set up event subscriptions

This tells our service when files are uploaded or changed.

```sh
gsutil notification create -t storage-updates -f json gs://gyldendal-flexafvikler.appspot.com
```

### Cloud Run Pub/Sub service on GCP

Cloud-Run does by default only invoke on HTTP requests. If we want to run pre-processing, we need to setup Google Cloud to invoke Cloud Run on pub/sub messages.

https://medium.com/google-cloud/cloud-run-as-an-internal-async-worker-480a772686e

#### Enable your projects to create Cloud Pub/Sub authentication tokens

```sh
gcloud projects add-iam-policy-binding cloudrun-atamel --member=serviceAccount:service-165810103461@gcp-sa-pubsub.iam.gserviceaccount.com --role=roles/iam.serviceAccountTokenCreator
```

#### Create a service account to represent the Cloud Pub/Sub subscription identity

```sh
gcloud iam service-accounts create cloud-run-pubsub-invoker --display-name "Cloud Run Pub/Sub Invoker"
```

#### Give this service account permission to invoke your CloudRun service

```sh
gcloud beta run services add-iam-policy-binding vision-csharp --member=serviceAccount:cloud-run-pubsub-invoker@cloudrun-atamel.iam.gserviceaccount.com --role=roles/run.invoker
```

### Rewrite to cloud-run from Firebase

Maybe useful for something like production, where we don't want to show the service.

https://firebase.google.com/docs/hosting/full-config?authuser=1#direct_requests_to_a_cloud_run_container

## Run locally

## Install dependencies

### Mac

```sh
brew install ffmpeg vips
```

### Configure application credentials locally

This creates a credential files for your machine that tmbserver can detect in order to access resources on GCP.

```sh
gcloud auth application-default login
```

### Run

Create a .env file with the following variables. No comments in this file are allowed.

```
TMBSERV_DBUCKET=gs://destination-bucket-url
TMBSERV_SBUCKET=gs://source-bucket-url
```

```sh
export $(cat .env | xargs) && go run cmd/tmbserv/tmbserv.go
```

### Test in a Docker container

The service-account.json can be downloaded from Cloud Console in IAM/service-accounts

```sh
docker run -ti -e TMB_DST=gs://tmbdata.gyldendal.redia.dk/ -e TMB_SRC=gs://gyldendal-flexafvikler.appspot.com/uploads/,gs://gyldendal-teknologi-prod.appspot.com/uploads/ -e PORT=8080 -v $(pwd)/service-account.json:/app/service-account.json -e GOOGLE_APPLICATION_CREDENTIALS=/app/service-account.json -p 8080:8080 tmbserv
```
