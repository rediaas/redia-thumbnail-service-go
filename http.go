package thumbnails

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

var (
	assetPattern = regexp.MustCompile(`\/r\/(.*)$`)
)

func getAssetURL(u string) (*url.URL, error) {
	match := assetPattern.FindStringSubmatch(u)
	if match == nil {
		return nil, errors.New("invalid asset url")
	}
	matchedURL := match[len(match)-1]
	// For some reason URLs eat one of the slashes. Maybe we should encode, but meh
	matchedURL = strings.Replace(matchedURL, ":/", "://", 1)

	imgURL, err := url.Parse(matchedURL)
	if err != nil {
		return nil, err
	}
	return imgURL, nil
}

// ImageHandler does all the heavy lifting.
func (s *Service) ImageHandler(w http.ResponseWriter, r *http.Request) {

	imgURL, err := getAssetURL(r.URL.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	opts := optionsFromPath(r.URL.Path)
	width := 0 // default width
	if opts["width"] != "" {
		wi, err := strconv.Atoi(opts["width"])
		if err == nil {
			width = wi
		}
	}
	// Check if we already have a thumbnail for the request
	optStruct := ImageOpts{
		Width: width,
	}

	if optStruct.Width == 0 {
		optStruct.Width = 1920 // Not sure if it's a good idea to default this, but errors are so mean
	}

	// This creates an URL that contains both bucket, base path and requested path

	a, err := s.NewAsset(r.Context(), imgURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return

	}

	err = a.WriteThumbnail(w, optStruct)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// VideoHandler outputs videos for assets
func (s *Service) VideoHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Videohandler called for", r.URL.Path)
	assetURL, err := getAssetURL(r.URL.Path)
	if err != nil {
		fmt.Println("Error while calling getAssetURL:", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	a, err := s.NewAsset(r.Context(), assetURL)
	if err != nil {
		fmt.Println("Error while calling newasset:", err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return

	}

	err = a.WriteVideo(w, r, Video720P)
	if err != nil {
		fmt.Println("Error while calling writeVideo:", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// TODO: Parse options, while we only support 720p, this is easy
}

func optionsFromPath(path string) map[string]string {
	m := make(map[string]string)
	regKeyVal := regexp.MustCompile(`([a-zA-Z0-9]+)=([a-zA-Z0-9]+)`)

	canditates := strings.Split(path, "/")
	for _, v := range canditates {
		if regKeyVal.MatchString(v) {
			match := regKeyVal.FindStringSubmatch(v)
			if len(match) == 3 {
				m[match[1]] = match[2]
			}
		}
	}

	return m
}
