package thumbnails

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"
)

// Asset is some sort of media that needs thumbnailing
// - It has sourceImage local file-path. If empty, it needs to be generated
type Asset struct {
	Name       string   // Relative name
	Mime       string   // mime-type by file-name (done when New is run)
	URL        *url.URL // Source original
	DstBaseURL *url.URL // Where to upload thumbnails (path, not filename)
	// tmpSrc is a local temporary file, if empty it needs to be generated
	ctx context.Context
	s   *Service
}

// NewAsset creates a new Asset instance. Does not open anything.
func (s *Service) NewAsset(ctx context.Context, src *url.URL) (*Asset, error) {
	a := Asset{
		s:   s,
		URL: src,
		ctx: ctx,
	}

	// This protects our bucket resources from leaking. We only accept full URLs if they are whitelisted first
	if !s.IsValidRequestURL(src) {
		return nil, errors.New("Requested URL is not whitelisted, add as source before trying to access")
	}
	if !src.IsAbs() {
		return nil, errors.New("Relative paths have been disabled in order to support multiple buckets")
	}
	a.Name = strings.Trim(src.Path, "/")
	a.Mime = mime.TypeByExtension(path.Ext(a.Name))

	// TODO Enforce supported mime types
	return &a, nil
}

// Returns a path to a temporary file or error
func (s *Service) imageAsset(r io.Reader) (string, error) {
	tmp, err := ioutil.TempFile("", "tmb_imgtmp")
	if err != nil {
		return "", err
	}

	_, err = io.Copy(tmp, r)
	if err != nil {
		return "", err
	}
	defer tmp.Sync()
	defer tmp.Close()

	return tmp.Name(), nil
}

func (s *Service) videoAsset(r io.Reader) (string, error) {
	vf, err := ioutil.TempFile("", "tmb_videotmp")
	if err != nil {
		return "", err
	}

	defer vf.Close()

	imf, err := ioutil.TempFile("", "tmp_imgtmp")
	if err != nil {
		return "", err
	}
	defer imf.Close()

	_, err = io.Copy(vf, r)
	if err != nil {
		return "", err
	}
	vf.Sync()
	defer os.Remove(vf.Name()) // delete the video file afterwards

	cmd := exec.Command("ffmpeg", "-i", vf.Name(), "-vframes", "1", "-ss", "0.5", "-s", "1920x1080", "-f", "singlejpeg", "-")
	cmd.Stdout = imf
	var out bytes.Buffer
	cmd.Stderr = &out // todo: store in a buffer and show with error
	err = cmd.Run()
	if err != nil {
		fmt.Println(out.String())
		return "", err
	}

	return imf.Name(), nil
}

// IsValidRequestURL helps guard the service against misuse. Used in HTTP.
func (s *Service) IsValidRequestURL(src *url.URL) bool {
	for _, v := range s.sources {
		if v.String() == "" {
			continue
		}
		if strings.HasPrefix(src.String(), v.String()) {
			return true
		}
	}
	return false
}

// CachePath generates a filename used by the cache bucket
func (a *Asset) CachePath(opts ImageOpts) string {
	b := a.URL.String()
	if opts.Width != 0 {
		b = fmt.Sprintf("[w-%d]%s", opts.Width, b)
	}

	h := sha256.New()
	h.Write([]byte(b))
	return "cache/" + hex.EncodeToString(h.Sum(nil))
}

// IsVideo tells if the asset is a video or not.
// Used to determine what commands to run on an asset
func (a *Asset) IsVideo() bool {
	switch a.Mime {
	case "video/mp4":
		return true
	default:
		return false
	}
}

// IsImage returns false if not supported image
func (a *Asset) IsImage() bool {
	switch a.Mime {
	case "image/jpeg", "image/png", "image/gif":
		return true
	default:
		return false
	}
}

// WriteThumbnail writes a single thumbnail to a io.Writer with the given options
// - This is used by the HTTP handler
func (a *Asset) WriteThumbnail(w io.Writer, opt ImageOpts) error {
	b, err := a.s.OpenBucket(a.ctx, a.s.destURL.String())
	if err != nil {
		return err
	}
	tmb := a.CachePath(opt)
	exists, err := b.Exists(a.ctx, tmb)
	if err != nil {
		return err
	}

	// Return a reader to the thumbnail if found
	if exists {
		r, err := b.NewReader(a.ctx, tmb, nil)
		if err != nil {
			return err
		}
		defer r.Close()
		stime := time.Now()
		_, err = io.Copy(w, r)
		fmt.Printf("HTTP: %s %v %s [cached]\n", a.URL.String(), opt, time.Since(stime))
		return err

	}

	// Thumbnail does not exist
	sfname, err := a.getSourceImage()
	if err != nil {
		return err
	}
	defer os.Remove(sfname)

	stime := time.Now()
	err = a.ProcessImage(sfname, opt, w)
	if err != nil {
		return err
	}
	fmt.Printf("HTTP: %s %v %s [processed]\n", a.URL.String(), opt, time.Since(stime))
	return nil
}

// getSourceImage returns a filename of a source file.
// For videos the filename isn't the video, but an image representation of the video
// Note: You need to clean-up the filename returned by this function
func (a *Asset) getSourceImage() (string, error) {
	var bucketURL = *a.URL

	b, err := a.s.OpenBucket(a.ctx, bucketURL.String())
	if err != nil {
		return "", err
	}
	defer b.Close()

	// Open reader from bucket
	r, err := b.NewReader(a.ctx, a.Name, nil)
	if err != nil {
		return "", err
	}
	defer r.Close()

	tmpfile := ""
	switch {
	case a.IsImage():
		t, err := a.s.imageAsset(r)
		if err != nil {
			return "", err
		}
		tmpfile = t
	case a.IsVideo():
		t, err := a.s.videoAsset(r)
		if err != nil {
			return "", err
		}
		tmpfile = t
	default:
		return "", errors.New("Unsupported mime type: " + a.Mime)
	}

	if tmpfile == "" {
		return "", errors.New("source-image path cannot be empty, something is wrong")
	}

	return tmpfile, nil
}

// WriteVideo writes a video according to a preset or falls back onto original
func (a *Asset) WriteVideo(w http.ResponseWriter, r *http.Request, preset VideoPreset) error {
	fmt.Println("Writevideo starts", a.Name)
	prefix := preset.String()
	dname := fmt.Sprintf("%s/%s", prefix, a.Name)

	db, err := a.s.OpenBucket(a.ctx, a.s.destURL.String())
	if err != nil {
		return err
	}
	defer db.Close()

	ex, err := db.Exists(a.ctx, dname)
	if err != nil {
		return err
	}
	if ex { // Pre-processed video exists
		if a.s.useRedirect {
			u, err := db.SignedURL(a.ctx, dname, nil)
			if err != nil {
				return err
			}
			http.Redirect(w, r, u, http.StatusTemporaryRedirect)
			fmt.Fprintf(w, "You should've been redirected to: %s", u)
			return nil
		}
		r, err := db.NewReader(a.ctx, dname, nil)
		if err != nil {
			return err
		}
		defer r.Close()
		_, err = io.Copy(w, r)
		return err
	}
	// Pipe the source image if no downscaled version is found

	sb, err := a.s.OpenBucket(a.ctx, a.URL.String())
	if err != nil {
		return err
	}
	defer sb.Close()

	if a.s.useRedirect {
		u, err := sb.SignedURL(a.ctx, a.Name, nil)
		if err != nil {
			return err
		}
		http.Redirect(w, r, u, http.StatusTemporaryRedirect)
		return nil
	}

	fmt.Println("Attempting to pipe the original")

	sr, err := sb.NewReader(a.ctx, a.Name, nil)
	if err != nil {
		fmt.Println("Error from reader", err.Error())
		return err
	}
	defer sr.Close()

	fmt.Println("copying...")
	_, err = io.Copy(w, sr)
	if err != nil {
		fmt.Println("error from io copy", err.Error())
		return err
	}

	return nil
}

// ProcessImage processes the source-file according to options and uploads it
// baseURL is where to put the files (destination bucket url)
// if w is defined, the thumbnail will also be written to that. Otherwise, only to the destination bucket
func (a *Asset) ProcessImage(sfname string, opt ImageOpts, w io.Writer) error {
	tmb := a.CachePath(opt)
	b, err := a.s.OpenBucket(a.ctx, a.s.destURL.String())
	if err != nil {
		return err
	}
	defer b.Close()
	srcFile, err := os.Open(sfname)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	bw, err := b.NewWriter(a.ctx, tmb, nil)
	if err != nil {
		return err
	}
	defer bw.Close()

	// If we need to write the output to a receiver, like http response
	var mw io.Writer
	if w == nil {
		mw = bw
	} else {
		mw = io.MultiWriter(w, bw)
	}

	ext := "jpg" // use jpg as asset format by default
	if a.Mime == "image/gif" {
		// Gif work-arounds
		ext = "gif"
		if opt.Width > 480 {
			// Gif sucks as a fileformat
			opt.Width = 480
		}
	}

	err = ScaleByWidth(mw, srcFile, opt.Width, ext)
	if err != nil {
		return err
	}
	return nil
}
